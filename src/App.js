import React, { Fragment, useState, useEffect } from "react";
import "./App.css";
import { Wrapper } from "./components/wrapper";
import { Header } from "./components/header";

// let logedIn = localStorage.getItem("logedIn");
// let users = JSON.parse(localStorage.getItem("users"));

// if (!logedIn || !users || (users && !users.find((e) => e.name === logedIn))) {
//   window.location.href = "./signin.html";
// }

const App = () => {
  const memoryTask = localStorage.getItem("tasks");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [list, setList] = useState(memoryTask ? JSON.parse(memoryTask) : []);

  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(list));
  }, [list]);

  const onAddTask = () => {
    const task = { name, description, id: Math.round(Math.random() * 1000) };
    setDescription("");
    setName("");
    const _list = [...list];
    _list.push(task);
    setList(_list);
  };

  const onDeleteTask = (id) => {
    const _list = [...list];
    const index = _list.findIndex((task) => task.id === id);
    _list.splice(index, 1);
    setList(_list);
  };

  const onEditDescription = (e, id) => {
    const _list = [...list];
    const index = _list.findIndex((task) => task.id === id);
    _list[index].description = e.target.value;
    setList(_list);
  };

  const renderTasks = () =>
    list.map(({ name, description, id }, i) => (
      <div className="allTasks" key={id}>
        <h5>{i + 1 + "." + " " + name}</h5>
        <textarea
          className="diskriptionTasks"
          value={description}
          onChange={(e) => onEditDescription(e, id)}
        />
        <button onClick={() => onDeleteTask(id)}>delete</button>
        <hr />
      </div>
    ));

  const setListners = (collection) => {
    console.log(collection);
    collection.forEach = [].forEach;
    collection.forEach((el) => {
      el.addEventListener("input", () => {
        el.style.height = `${el.scrollHeight}px`;
      });
    });
  };

  useEffect(() => {
    const areas = document.getElementsByClassName("diskriptionTasks");

    setListners(areas);
  }, []);

  return (
    <Fragment>
      {<Header />}
      <Wrapper>
        <h1>To do list</h1>
        <div className="flex">
          <div className="container">
            <input
              className="name"
              placeholder="Task name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <textarea
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              className="description"
              placeholder="Description"
            />
            <button onClick={onAddTask} disabled={!name || !description}>
              Add
            </button>
            <h4>unfinished tasks</h4>
          </div>
          <div className="tasklist">{renderTasks()}</div>
        </div>
      </Wrapper>
      {/* <Table name="NNN" /> */}
    </Fragment>
  );
};

export default App;
