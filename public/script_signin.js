
let users;

if (localStorage.getItem("users")) {
  users = JSON.parse(localStorage.getItem("users"));
} else {
  localStorage.setItem("users", JSON.stringify([]));
  users = [];
}

let submit = document.getElementById("button_signin");
let login = document.getElementById("login_signin");
let password = document.getElementById("pass_signin");

const onSubmit = () => {
  let _login = login.value;
  let _password = password.value;
  if (!_login || !_password) {
    alert("Неверное имя пользователя или пароль");
    return;
  }
  let user = users.find((el) => el.name === _login);
  if (user && user.password === _password) {
    localStorage.setItem("logedIn", user.name);
    document.location.href = 'http://localhost:3000/';
  } else if (user && user.password !== _password) {
    alert("wrong password");
  } else {
    alert("User does not exist");
  }
};

submit.addEventListener("click", onSubmit);